# ATB Dev

## Projects

- Countdown
- More coming soon...

### Countdown

Countdown is a timer that counts down the time left of an event/stream. Tiping, cheering or subscribing allows users to add/remove time.

When a user tips/cheers they must tip/cheer a minimum of $5/500 bits and the message must include one of the two hashtags #addtime or #removetime. If done correctly the system will add/remove 10 minutes to/from the countdown.

When a user resubs they must include one of the two hashtags (#addtime or #removetime) in their resub chat message in order to add/remove 10 minutes to/from the countdown.

When a user subscribes for the first time or resubs after they lose their sub streak they must include one of the two hashtags (#addtime or #removetime) in their next chat message in order to add/remove 10 minutes to/from the countdown.

#### Setup

- Make sure to edit the config.js in the countdown folder with your correct details before use.
- Load the html file as a local file on OBS BrowserSource at 800x600 or resize as you see fit
- You can change font color and font size in css file

##### Tip Requirements

- $5 minimum
- User's tip message must include #addtime or #removetime

##### Cheer Requirements

- 500 bits minimum
- User's cheer message must include #addtime or #removetime

##### Resub Requirements

- User's resub chat message must include #addtime or #removetime

##### New Subscriber Requirements

- User's next chat message must include #addtime or #removetime