var parse = function(data) {
    var message = {
        raw: data,
        tags: {},
        prefix: null,
        command: null,
        params: []
    }

    // position and nextspace are used by the parser as a reference.
    var position = 0
    var nextspace = 0

    // The first thing we check for is IRCv3.2 message tags.
    // http://ircv3.atheme.org/specification/message-tags-3.2

    if (data.charCodeAt(0) === 64) {
        var nextspace = data.indexOf(' ')

        if (nextspace === -1) {
            // Malformed IRC message.
            return null
        }

        // Tags are split by a semi colon.
        var rawTags = data.slice(1, nextspace).split(';')

        for (var i = 0; i < rawTags.length; i++) {
            // Tags delimited by an equals sign are key=value tags.
            // If there's no equals, we assign the tag a value of true.
            var tag = rawTags[i]
            var pair = tag.split('=')
            message.tags[pair[0]] = pair[1] || true
        }

        position = nextspace + 1
    }

    // Skip any trailing whitespace.
    while (data.charCodeAt(position) === 32) {
        position++
    }

    // Extract the message's prefix if present. Prefixes are prepended
    // with a colon.

    if (data.charCodeAt(position) === 58) {
        nextspace = data.indexOf(' ', position)

        // If there's nothing after the prefix, deem this message to be
        // malformed.
        if (nextspace === -1) {
            // Malformed IRC message.
            return null
        }

        message.prefix = data.slice(position + 1, nextspace)
        position = nextspace + 1

        // Skip any trailing whitespace.
        while (data.charCodeAt(position) === 32) {
            position++
        }
    }

    nextspace = data.indexOf(' ', position)

    // If there's no more whitespace left, extract everything from the
    // current position to the end of the string as the command.
    if (nextspace === -1) {
        if (data.length > position) {
            message.command = data.slice(position)
            return message
        }

        return null
    }

    // Else, the command is the current position up to the next space. After
    // that, we expect some parameters.
    message.command = data.slice(position, nextspace)

    position = nextspace + 1

    // Skip any trailing whitespace.
    while (data.charCodeAt(position) === 32) {
        position++
    }

    while (position < data.length) {
        nextspace = data.indexOf(' ', position)

        // If the character is a colon, we've got a trailing parameter.
        // At this point, there are no extra params, so we push everything
        // from after the colon to the end of the string, to the params array
        // and break out of the loop.
        if (data.charCodeAt(position) === 58) {
            message.params.push(data.slice(position + 1))
            break
        }

        // If we still have some whitespace...
        if (nextspace !== -1) {
            // Push whatever's between the current position and the next
            // space to the params array.
            message.params.push(data.slice(position, nextspace))
            position = nextspace + 1

            // Skip any trailing whitespace and continue looping.
            while (data.charCodeAt(position) === 32) {
                position++
            }

            continue
        }

        // If we don't have any more whitespace and the param isn't trailing,
        // push everything remaining to the params array.
        if (nextspace === -1) {
            message.params.push(data.slice(position))
            break
        }
    }
    return message
}

/*function parse(raw){
	var data = {}
	var chunks = raw.split(' ')
	var typeSource = raw.split(' ', 4)
	var typeList = ['PRIVMSG', 'CLEARCHAT', 'ROOMSTATE', 'NOTICE']
	var type = typeSource.find(typeList)

	if(type == 'PRIVMSG'){
		var part = chunks[0].split(';')

		data.color = part[0].split('=')[1]
		data.display_name = part[1].split('=')[1]
		data.username = part[1].split('=')[1].toLowerCase()

		if(part[2].split('=')[1] !== ''){
			data.emotes = []

			var sets = part[2].split('=')[1].split('/')
			
			for(var i = 0;i<sets.length;i++){
				var emote = {
					id: sets[i].split(':')[0],
					cords: []
				}
				var positions = sets[i].split(':')[1].split(',')

				for(var x = 0;x<positions.length;x++){
					var cords = positions[x].split('-')
					emote.cords = {
						start: cords[0],
						end: cords[1]
					}
					data.emotes.push(emote)
				}
			}
			
		}else {
			data.emotes = ''
		}


		if(part[3].split('=')[1] == 1){
			data.subscriber = true;
		}else{
			data.subscriber = false;
		}

		if(part[4].split('=')[1] == 1){
			data.turbo = true;
		}else{
			data.turbo = false;
		}
		data.role = part[5].split('=')[1]
		
		data.type = 'PRIVMSG'
		data.channel = chunks[3]
		data.message = chunks.slice(4).join(' ').slice(1);
	}

	if(type == 'ROOMSTATE'){

		var part = chunks[0].split(';')

		if(part.length > 1){
			data.type = 'ROOMSTATE';
			data.channel = chunks[3];

			data.lang = part[0].split('=')[1]
			
			if(part[1].split('=')[1] == 1){
				data.r9k = true;
			}else{
				data.r9k = false;
			}

			if(part[2].split('=')[1] == 1){
				data.slow = true;
			}else{
				data.slow = false;
			}

			if(part[3].split('=')[1] == 1){
				data.subs_only = true;
			}else{
				data.subs_only = false;
			}
		}
	}

	if(type == 'CLEARCHAT'){
		data.type = 'CLEARCHAT';
		data.channel = chunks[2];
		if(chunks.length > 3){
			data.username = chunks[3];
		}
	}

	return data;
}
*/