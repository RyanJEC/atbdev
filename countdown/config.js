let _ = {
	stream: 'twitch', //Must be twitch username. http://twitch.tv/twitch
	max_Time: 16, // max time in hours.
	start_Time: 12, //Countdown start time in hours.
	minutes: 10, //Number minutes to add/remove per tip/cheer/sub at minimum requirements.
	default_service: 'streamlabs', // muxy or streamlabs
	muxyID: '', /* Must be muxy id at the end of muzy overlay url. http://a.muxy.io/alert/twitch/ZXTf_ILqCbU-NjnxHlz29PWMcF4srOz*/
	streamlabs_SocketToken: '' // token located under streamlabs API Settings > API Tokens > Your Socket API Token
}