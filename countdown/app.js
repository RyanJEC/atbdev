//start timer at 12 hours
let Timer = moment.duration(_.start_Time, 'hours');

let TimerTracker = moment.duration(_.start_Time, 'hours')
//how many minutes to add/remove per 500 cheer, $5 tip, or sub
let timeChange = _.minutes;

//endedTimer switch
let endedTimer = false;

//writes timer to html
updateHTML();

//interval that runs ever second
window.setInterval(() => {
	removeSecond()
	/*if (!endedTimer) {
	//subtracts 1 second from the timer and updates html
	removeSecond()
} else {
	//adds 1 second to timer when reaches zero and updates html
	addSecond();
}
*/ //checks if the timer has reached zero
	if (Timer.asSeconds() <= 0) {
		//endedTimer switches to true
		endedTimer = true
	}

}, 1000)

let socket = null;

function createStreamlabsSocket() {
	socket = io('https://sockets.streamlabs.com?token=' + _.streamlabs_SocketToken);

	socket.on('connect', () => {
		console.log('Streamlabs Connected!')
	})

	socket.on('error', (error) => {
		console.log(error)
	})

	socket.on('disconnect', (reason) => {
		console.log('Streamlabs Disconnected! - ' + reason)
	})

	socket.on('event', (eventData) => {
		alertRead(eventData);
	});
}

function createMuxySocket() {
	//Muxy Socket
	socket = new WebSocket('ws://a.muxy.io/ws/' + _.stream + '/' + _.muxyID);

	socket.addEventListener('open', (e) => {
		console.log('Muxy Connected!')
	})

	socket.addEventListener('message', (e) => {
		//console.log(e.data);
		let data = JSON.parse(e.data);
		alertRead(data);
	})

	socket.addEventListener('error', (e) => {
		console.log(e.data)
	})

	socket.addEventListener('close', (e) => {
		console.log('Muxy Disconnected!')
	})
}

if (_.default_service.toLowerCase() == 'streamlabs') {
	createStreamlabsSocket();
} else if (_.default_service.toLowerCase() == 'muxy') {
	createMuxySocket();
}


let chat = null;

//Twitch Chat Socket
function createTwitchSocket() {
	chat = new WebSocket('ws://irc-ws.chat.twitch.tv');

	chat.addEventListener('open', (e) => {
		console.log('Chat Connected!')
		chat.send('CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership\r\n');
		chat.send('NICK justinfan9000\r\n');
		chat.send('JOIN #' + _.stream + '\r\n');
	})

	chat.addEventListener('message', (e) => {
		//console.log(e.data);
		splitData(e.data)
	})

	chat.addEventListener('error', (e) => {
		console.log(e.data)
	})

	chat.addEventListener('close', (e) => {
		console.log('Chat Disconnected!')
	})
}

createTwitchSocket();


function alertRead(data) {
	if (_.default_service.toLowerCase() == 'streamlabs') {

		if (!data.for && data.type === 'donation') {
			countTip(data);
			console.log(data.message);
		}
		if (data.for === 'twitch_account') {
			switch (data.type) {
				case 'bits':
					countCheer(data);
					console.log(data.message);
					break;
				case 'subscription':
					countSubscriber(data);
					console.log(data.message);
					break;
				default:
					//default case
					console.log(data.message);
			}
		}

	}else if (_.default_service.toLowerCase() == 'muxy') {
		if (data.type === 'follow') { } else if (data.type === 'points_given') {
			countCheer(data);
		} else if (data.type === 'subscribe') {
			countSubscriber(data);
		} else if (data.type === 'donate') {
			countTip(data);
		} else if (data.type === 'hosted') { } else { }
	}
}

function countCheer(data) {
	if (_.default_service.toLowerCase() == 'streamlabs') {

		if (data.message[0].message.toLowerCase().includes("#addtime") && data.message[0].amount >= 500) {
			addTime()
		} else if (data.message[0].message.toLowerCase().includes("#removetime") && data.message[0].amount >= 500) {
			removeTime()
		}

	}else if (_.default_service.toLowerCase() == 'muxy') {
		if (data.user_message.toLowerCase().includes("#addtime") && data.extra.amount >= 500) {
			addTime()
		} else if (data.user_message.toLowerCase().includes("#removetime") && data.extra.amount >= 500) {
			removeTime()
		}
	}
}

function countTip(data) {
	if (_.default_service.toLowerCase() == 'streamlabs') {

		if (data.message[0].message.toLowerCase().includes("#addtime") && data.message[0].amount >= 5) {
			addTime()
		} else if (data.message[0].message.toLowerCase().includes("#removetime") && data.message[0].amount >= 5) {
			removeTime()
		}

	}else if (_.default_service.toLowerCase() == 'muxy') {
		if (data.user_message.toLowerCase().includes("#addtime") && data.extra.amount >= 5) {
			addTime()
		} else if (data.user_message.toLowerCase().includes("#removetime") && data.extra.amount >= 5) {
			removeTime()
		}
	}
}

function countSubscriber(data) {
	if (_.default_service.toLowerCase() == 'streamlabs') {

		if (data.message[0].message.toLowerCase().includes("#addtime")) {
			addTime()
		} else if (data.message[0].message.toLowerCase().includes("#removetime")) {
			removeTime()
		}
		

	}else if (_.default_service.toLowerCase() == 'muxy') {
		if (data.user_message == "") {
		} else {
			if (data.user_message.toLowerCase().includes("#addtime")) {
				addTime()
			} else if (data.user_message.toLowerCase().includes("#removetime")) {
				removeTime()
			} else {
				addTime()
			}
		}
	}
}

//FUNCTIONS

//adds "0" padding to hours, minutes, and seconds
function padWithZero(input, length) {
	// Cast input to string
	input = "" + input;

	let paddingSize = Math.max(0, length - input.length);
	return new Array(paddingSize > 0 ? paddingSize + 1 : 0).join("0") + input;
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

function addTime(time) {
	if (TimerTracker.hours() < _.max_Time) {
		if (time) {
			Timer.add(time, 'm')
			TimerTracker.add(time, 'm')
		} else {
			Timer.add(timeChange, 'm')
			TimerTracker.add(timeChange, 'm')
		}
	}
	updateHTML();
}

function removeTime(time) {
	if (time) {
		Timer.subtract(time, 'm')
		TimerTracker.subtract(time, 'm')
	} else {
		Timer.subtract(timeChange, 'm')
		TimerTracker.subtract(timeChange, 'm')
	}
	updateHTML();
}

function addSecond() {
	Timer.add(1, 's')
	updateHTML();
}

function removeSecond() {
	Timer.subtract(1, 's')
	updateHTML();
}

function updateHTML() {
	if (!endedTimer) {
		$('.timer').html(padWithZero(Math.floor(Timer.asHours()), 2) + ':' + padWithZero(Timer.minutes(), 2) + ':' + padWithZero(Timer.seconds(), 2))
	} else {
		$('.timer').html('-' + padWithZero(Math.floor(Math.abs(Timer.asHours())), 2) + ':' + padWithZero(Math.abs(Timer.minutes()), 2) + ':' + padWithZero(Math.abs(Timer.seconds()), 2))
	}
}

function splitData(rawData) {
	let rawArray = rawData.split('\r\n')
	for (let data of rawArray) {
		if (data !== '') {
			parseCommand(parse(data));
		}
	}
}

function parseCommand(data) {
	if (data.command == 'PING') {
		ping();
	}
	if (data.command == 'PRIVMSG') {
		privmsg(data);
	}
}

function ping() {
	chat.send('PONG :tmi.twitch.tv\r\n');
}

function privmsg(data) {
	//console.log(data)

	let user = data.prefix.split('!')[0];
	let command = data.params[1].split(' ')[0].toLowerCase();

	/* if (subList.includes(user)) {
		subList.remove(user)
		if (data.params[1].toLowerCase().includes("#addtime")) {
			addTime()
		} else if (data.params[1].toLowerCase().includes("#removetime")) {
			removeTime()
		}
	} */

	if (user == _.stream && command == '!addtime') {
		let time = parseInt(data.params[1].split(' ')[1]);
		if (typeof time === 'number' && time > -1) {
			addTime(time)
		} else {
			addTime()
		}
	}

	if (user == _.stream && command == '!removetime') {
		let time = parseInt(data.params[1].split(' ')[1]);
		if (typeof time === 'number' && time > -1) {
			removeTime(time)
		} else {
			removeTime()
		}
	}

	if (user == _.stream && command == '!restartmuxy') {
		socket.close();
		if (_.default_service.toLowerCase() == 'streamlabs') {
			createStreamlabsSocket();
		} else if (_.default_service.toLowerCase() == 'muxy') {
			createMuxySocket();
		}
	}

	/*if(user == _.stream && data.params[1].slice(0,1) == '!'){
		let commandData = data.params[1].slice(1).split(' ', 3);
		let command = commandData[0];
		let value = data.params[1].slice(1).split(' ', 1);
	}*/
}

function testTip(name, amount, message) {
	let data = {
		"type": "donate",
		"user_message": message,
		"viewer": {
			"name": name
		},
		"extra": {
			"amount": amount,
		}
	}
	countTip(data)
}

function testSub(name, message) {
	let data = {
		"type": "donate",
		"user_message": message,
		"viewer": {
			"name": name
		}
	}
	countSubscriber(data)
}

function testCheer(name, amount, message) {
	let data = {
		"type": "donate",
		"user_message": message,
		"viewer": {
			"name": name
		},
		"extra": {
			"amount": amount,
		}
	}
	countCheer(data)
}

Array.prototype.remove = function (string) {
	let index = this.indexOf(string);
	this.splice(index, 1);
}

/*function randomTime() {
	let num = getRandomInt(0, 1);
	if (num == 0) {
		addTime()
	} else if (num == 1) {
		removeTime()
	}
}*/